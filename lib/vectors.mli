open Utils
open Naturals

module type Vec_shuffle = sig
  module Size: Nat
  val n_dims: int
    (*vec_size = 2 ** n_dims *)
  val vec_size: int

  (* Coordinate in Vector of size vec_size
   * Actually a tuple of size n = log2(vec_size) *)
  module C :
  sig
    include Coord.Coord_t with module S := Size
  end

  (* A mask taking a half of a vector
   * First element of the pair is the dimension on which we are discriminating,
   * second specifies which element we take (first or second).
   * For example (0, Low) in a vector of size 8 takes this part
   * [ ** , _ , ** , _ , ** , _ , ** , _ ]
   * where all ** are selected and _ are discarded.
   * With the same convention, (1, High) is
   * [ _ , _ , ** , ** , _ , _ , ** , ** ]
   * *)
  type mask_half = int * Coord.HighLow.t

(* This type represents an extraction operation.
 * Each extraction takes two vectors as input (left and right),
 * takes a half of each vector and stores both half into a result vector
 * in a way specified by the input *)
  type extract =
    (* Completetely generic extraction. Generic (left_mask, right_mask,
     * out_mask) select left_mask out of left input, right_mask out of right
     * input, and store elements selected from left input into out_mask, in
     * order.
     * (elements from right mask are therefore stored in (complement out_mask)
     * *)
      Generic of mask_half * mask_half * mask_half
    (* Same as the previous one, but we time we restrain the operation by
     * applying on the same dimensions on left and right. Elements
     * taken are still free. *)
    | Same_end of int * Coord.HighLow.t * Coord.HighLow.t * mask_half
    (* The dimension where we store the result is the same that
     * the one we discriminate on. *)
    | Par_shuffle of { keep : int; left : Coord.HighLow.t; right : Coord.HighLow.t;
                   out : Coord.HighLow.t;
                 }
    (* Same as the previous one, but in addition we alter the
     * order of elements in output (in the last version, the order
     * of elements coming from the same vector is preserved).
     * Here, the order will be inverted with respect to dimension inv.
     * inv is assumed to be different of keep.
     * For example
     * Shuffle{keep=1, inv=0 left = Low; right = Low; out =(0, High)}
     * on l=[0; 1; 2; 3; 4; 5; 6; 7] r=[8; 9; 10; 11; 12; 13; 14; 15] yields :
     * res=[9; 1; 8; 0; 3; 11; 2; 10 ].
     * keep = 2 and left = Low,
     * so we select [ ** ; ** ; ** ; ** ; _ ; _ ; _ ; _ ] in left vector. Same
     * for right vector
     * As we specified out = (0, High) all
     * elements coming from l are on [ _ ; ** ; _ ; ** ] in res.
     * Finally, as inv = 0, all first coordinate of elements of both vectors are
     * inverted and then this inverted order is preserved in output (even if the
     * dimensions change *)
    | Shuffle of { keep : int; inv : int; left : Coord.HighLow.t;
                   right : Coord.HighLow.t; out : mask_half; } [@@deriving show]

    (* Given a position (a coordinate c_out) and an extraction operation
     * prev_pos will return Left c_in if the element at position c_out is coming
     * from the left input of the extraction, or Right c_in if it is coming from
     * the right input.
     * In both case, c_in is the coordinate of the element in the input vector. *)
  val prev_pos : C.t -> extract -> (C.t, C.t) Either.t

  (* A vector is either an input, or the result of an extraction *)
  type vector_expr =
      Vector of int
    (* Invert dimension of given vector.
     * For example, invert_dim [0; 1; 2; 3] 0 = [1; 0; 3; 2]
     * and invert_dim [0; 1; 2; 3] 1 = [2; 3; 0; 1] *)
    | Invert_dim of vector_expr * int
    (* Applying an extraction on the two given vectors *)
    | Extract of vector_expr * vector_expr * extract [@@deriving show]

  (* vector_e is the output of a sequence of vector shuffling.
   * from_vec_at_pos vector_e coord returns the index and the coordinates from
   * where this element comes from in input *)
  val from_vec_at_pos : vector_expr -> C.t -> int * C.t

  val show_vec: vector_expr -> string
  val compose: vector_expr array -> vector_expr array -> vector_expr array
end
module type RVec = sig
  (* Coordinate as a concrete tuple - For example HL.t * HL.t in the case of a
   * 4-elements vector. Coordinate are by default represented as a static list,
   * which is less practical in concrete case even if this allows to be generic
   * in the size of the tuple - hence the use of N_tuples *)
  type coord
    (* Coordinate as (HL.t, 'n) N_tuples.t *)
  type c_list
  val to_tuple: c_list -> coord
  val from_tuple: coord -> c_list
  val show: c_list -> string
end

(* Functor that takes a module of type Nat (Namely, Two, Three, Four...)
 * Nat cannot be zero though and returns the module manipulating vector operations
 * of size pow(2, Nat.num) *)
module V: functor (S : Nat) -> (Vec_shuffle with module Size = S)

(* V(Two) = Vectors operations of size four. provided for convenience *)
module Vec4: sig
  include module type of V(Two)
  type coord = Coord.HighLow.t * Coord.HighLow.t
  include RVec with type coord := coord and type c_list := C.t
end

(* V(Three) = Vectors operations of size eight. provided for convenience *)
module Vec8: sig
  include module type of V(Three)
  type coord = Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t
  include RVec with type coord := coord and type c_list := C.t
end

module Vec16: sig
  include module type of V(Four)
  type coord = Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t
  include RVec with type coord := coord and type c_list := C.t
end


(* Create a first-class module vector of size n
 * Fails if n is not a power of two.
 * Using first-class modules comes at the price of wide usability :
 * Vectors are only compatible with themselves, so for example in this case :
 * let module V1 = create_vec 16
 * let module V2 = create_vec 16
 * V1.vec_expr is not compatible with V2.vec_expr.
 * Try to use normal functor when possible, even if readability is harmed *)
val create_vec: int -> (module Vec_shuffle)

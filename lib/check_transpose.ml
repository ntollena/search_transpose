open Batteries
open Vectors
open Utils

module type Mat_dim = sig
  val inner: int
  val outer: int
end

module type Check_t = sig
  type shuffle
  val print_vectors: shuffle -> unit
  val print_move: shuffle -> unit
  val is_transpose: shuffle -> bool
end

module type Check_utils_t = sig
  type vector_expr
  type shuffle = vector_expr array -> vector_expr array
  val num_vec:int
  val print_origin: (int * vector_expr) list  -> unit
  val check_exchange: (int * vector_expr) list -> bool
end

module Common (Vec:Vec_shuffle)
    (Check_utils:Check_utils_t with type vector_expr := Vec.vector_expr)
  : Check_t with type shuffle = (Vec.vector_expr array -> Vec.vector_expr array)  = struct
  open Vec
  include Check_utils
  (* Given a shuffle specification operating over an array of vectors,
   * initializes num_vec vectors (with num_vec suitably chosen) and passes them
   * as arguments to trans, then turns the result as list along with list
   * indexes *)
  let out_vec_list trans =
    Array.init Check_utils.num_vec (fun i -> Vector i)
    |> trans
    |> Array.to_list

  let print_vectors trans =
    out_vec_list trans
    |> List.map show_vec
    |> List.iter print_endline

  let print_move trans =
    out_vec_list trans
    |> Utils.enumerate
    |> print_origin

  let is_transpose (trans: vector_expr array -> vector_expr array) =
    out_vec_list trans
    |> Utils.enumerate
    |> check_exchange
end

module Check_total_trans (Vec:Vec_shuffle) (M:Mat_dim):
  Check_utils_t with type vector_expr = Vec.vector_expr = struct
  include Vec
  type shuffle = vector_expr array -> vector_expr array

  let num_vec = int_ceil (M.inner * M.outer) vec_size

  let to_coord tar_base (i, ii) =
    ( i * vec_size + ii) / tar_base, (i * vec_size + ii) mod tar_base

  let from_out_coord (vi:int) (v:vector_expr) (coord: C.t) =
    let v_from, coord_from = from_vec_at_pos v coord in
    let isrc, idest = C.coord_to_int coord_from, C.coord_to_int coord in
    let lsrc, csrc = to_coord M.inner (v_from, isrc) in
    let ldest, cdest = to_coord M.outer (vi, idest) in
    vi, idest, ldest, cdest, v_from, isrc, lsrc, csrc

  let print_origin = List.iter
      (fun (vi, v) ->
         let print_placement coord =
           let v_dest, idest, ldest, cdest, v_from, isrc, lsrc, csrc =
             from_out_coord vi v coord in
           (*Have we correctly exchanged (i, j) ? *)
           Printf.printf "output: [%d; %d], src [ %d; %d]
          c3_dest: %d, %d c3_src: %d, %d\n" v_dest idest
             v_from isrc  ldest cdest lsrc csrc in
         List.iter print_placement C.all_coords)

  let check_exchange =
    List.for_all
      (fun (vi, v) ->
         let check_placement coord =
           let v_dest, idest, ldest, cdest, _, _, lsrc, csrc =
             from_out_coord vi v coord in
           (*Have we correctly exchanged (i, j) ? *)
           (ldest = csrc && cdest = lsrc)
           || (v_dest * vec_size + idest >= M.inner * M.outer) in
         List.for_all check_placement C.all_coords)
end

module Check (Vec:Vec_shuffle) (M:Mat_dim) = struct
  module ChUtils = Check_total_trans(Vec)(M)
  include Common(Vec)(ChUtils)
end

module Check_incomplete_utils (Vec:Vec_shuffle)(M:Mat_dim):
  Check_utils_t with type vector_expr = Vec.vector_expr =  struct

  (* Only thing we really include here is the definition of vector_expr *)
  include Vec
  type shuffle = vector_expr array -> vector_expr array

  let num_vec = M.outer

  let print_origin = List.iter
      (fun (vi, v) ->
         let print_placement coord =
           let idest = C.coord_to_int coord in
           let (v_from, coord_from) = from_vec_at_pos v coord in
           let isrc = C.coord_to_int coord_from in
           (*Have we correctly exchanged (i, j) ? *)
           if isrc < M.inner then
             Printf.printf "output: [%d; %d], src [ %d; %d] \n" vi idest
               v_from isrc  in
         List.iter print_placement C.all_coords)

  let check_exchange =
    List.for_all
      (fun (vi, v) ->
         let check_placement coord =
           let idest = C.coord_to_int coord in
           let (v_from, coord_from) = from_vec_at_pos v coord in
           let isrc = C.coord_to_int coord_from in
           (*Have we correctly exchanged (i, j) ? *)
           (idest = v_from && vi = isrc)
           || (isrc >= M.inner) || (idest >= M.outer) in
         List.for_all check_placement C.all_coords)
end

module Check_incomplete(Vec:Vec_shuffle) (M:Mat_dim) = struct
  module ChUtils = Check_incomplete_utils(Vec)(M)
  include Common(Vec)(ChUtils)
end

module Check4 = Check(Vec4)
module Check8 = Check(Vec8)

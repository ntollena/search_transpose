open Batteries
open Utils
open Naturals

module  HL = Coord.HighLow

module type Vec_shuffle = sig
  module Size: Nat
  val n_dims: int
  val vec_size: int
  module C :
  sig
    include Coord.Coord_t with module S := Size
  end
  type mask_half = int * Coord.HighLow.t
  type extract =
      Generic of mask_half * mask_half * mask_half
    | Same_end of int * Coord.HighLow.t * Coord.HighLow.t * mask_half
    | Par_shuffle of { keep : int;  left : Coord.HighLow.t; right : Coord.HighLow.t;
                   out : Coord.HighLow.t;
                 }
    | Shuffle of { keep : int; inv : int; left : Coord.HighLow.t; right : Coord.HighLow.t;
                   out : mask_half;
                 } [@@deriving show]

  val prev_pos : C.t -> extract -> (C.t, C.t) Either.t

  type vector_expr =
      Vector of int
    | Invert_dim of vector_expr * int
    | Extract of vector_expr * vector_expr * extract [@@deriving show]

  val from_vec_at_pos : vector_expr -> C.t -> int * C.t
  val show_vec: vector_expr -> string
  val compose: vector_expr array -> vector_expr array -> vector_expr array
end

module V(S: Nat): Vec_shuffle with module Size = S = struct
  module Size = S
  let n_dims = from_nat S.num
  let vec_size = 1 lsl n_dims

  module C = Coord.Coord(S)

  (* Take a half of a vector in the specified way.
   * For example 1 * low are all elements
   * (x, 0, z) for all x, z in {0, 1}
   * 2 * Low = (x, y, 1) forall x, y *)
  type mask_half =  int * HL.t [@@deriving show]

  let mask_compl (endian, hl) = HL.(
      match hl with
      | Low -> endian, High
      | High -> endian, Low
    )

  let mask_dim (endian, c) coord =
    if C.coord_at_dim coord endian = c
    then Some (C.coord_end_compl coord endian)
    else  None

  type extract =
    (* Generic h1, h2, ho represents an "extraction" operation on two vectors
    * May be too generic - impossible to implement on intel avx *)
    | Generic of mask_half * mask_half * mask_half
    (* This I am pretty confident we can handle it for all
     * configuration *)
    | Same_end of int * HL.t * HL.t * mask_half
    | Par_shuffle of {keep: int;  left: HL.t; right: HL.t; out: HL.t}
    (* Same as the previous one but additionally the elements of inputs will be
     * inverted in output on the inv dimension *)
    | Shuffle of {keep: int;  inv: int; left: HL.t; right: HL.t; out:
                    mask_half} [@@deriving show]

  let extract_mask_in vec_ext eith: mask_half =
    match vec_ext with
    | Generic (half_left, half_right, _) -> Either.choose eith half_left half_right
    | Par_shuffle {keep=endianess; left; right; _}
    | Same_end (endianess, left, right, _) ->
      (endianess, Either.choose eith left right)
    | Shuffle {keep; left; right; _} ->
      (keep, Either.choose eith left right)

  let extract_mask_out = function
    | Generic (_,_,half_out) | Same_end (_,_,_, half_out)
    | Shuffle {out = half_out;_} -> half_out
    | Par_shuffle {keep; out;_} -> (keep, out)

  let shuffle = function
    | Generic _ | Same_end _ | Par_shuffle _ -> None
    | Shuffle {inv;_} -> Some inv

  (* Given a coordinate in the output of an extraction, and e the element at coord
   * in output, returns Left pos if e was located at pos in left argument of
   * vec_ext, Right pos otherwise *)
  let prev_pos coord vec_ext: (C.t, C.t) Either.t =
    (* Elements coming from left vector in output *)
    let mask_out_left = extract_mask_out vec_ext in
    (* Elements coming from right vector in output *)
    let mask_out_right = mask_compl mask_out_left in
    (* Elements coming from left/right in vector *)
    let endian_left, c_left = extract_mask_in vec_ext Either.left in
    let endian_right, c_right = extract_mask_in vec_ext Either.right in
    (* If we have a shuffle, then the position in input vector will have to be
     * inverted with respect to the appropriate coordinate *)
    let inv = shuffle vec_ext in
    match mask_dim mask_out_left coord,
          mask_dim mask_out_right coord with
    | Some tup, None -> Left (C.inject_inv inv c_left tup endian_left)
    | None, Some tup -> Right (C.inject_inv inv c_right tup endian_right)
    | _ -> failwith "Inconsistent state"

  type vector_expr =
    | Vector of int (* id *)
    | Invert_dim of vector_expr * int
    | Extract of vector_expr * vector_expr * extract [@@deriving show]

  let rec from_vec_at_pos vector_e coord = match vector_e with
    | Vector id -> id, coord
    | Invert_dim (vexpr, dim_inv) -> from_vec_at_pos vexpr (C.invert_dim coord dim_inv)
    | Extract (vl, vr, ext) -> (match (prev_pos coord ext) with
        | Left coord_in -> from_vec_at_pos vl coord_in
        | Right coord_in -> from_vec_at_pos vr coord_in)

  let show_vec v =
    let show_placement coord =
      let v_from, coord_src = from_vec_at_pos v coord in
      let pos = v_from * vec_size + (C.coord_to_int coord_src) in
      string_of_int pos in
    List.map show_placement C.all_coords
    |> String.concat ", "
    |> fun s -> "[" ^ s ^ "]"

  let compose vec_expr1 vec_expr2 =
    assert (Array.length vec_expr1 = Array.length vec_expr2);
    Array.map ( function
      | Vector i -> vec_expr1.(i)
      | Extract (Vector i1, Vector i2, ext) -> Extract (vec_expr1.(i1), vec_expr1.(i2), ext)
      | Invert_dim (Vector i, dim) -> Invert_dim (vec_expr1.(i), dim)
      | _ -> assert false
      ) vec_expr2

end

module type RVec = sig
  type coord
  type c_list
  val to_tuple: c_list -> coord
  val from_tuple: coord -> c_list
  val show: c_list -> string
end

(*Definition for 4-elements vectors and 8-elements vectors - the most common
 * ones - so they are ready for use *)
module Vec4: sig
  include module type of V(Two)
  type coord = Coord.HighLow.t * Coord.HighLow.t
  include RVec with type coord := coord and type c_list := C.t
end = struct
  include V(Two)
  type coord = Coord.HighLow.t * Coord.HighLow.t [@@deriving show]
  let to_tuple = N_tuples.flatten2
  let from_tuple = N_tuples.from2
  let show c = [%show: coord] (to_tuple c)
end

module Vec8: sig
  include module type of V(Three)
  type coord = Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t
  include RVec with type coord := coord and type c_list := C.t
end = struct
  include V(Three)
  type coord = Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t [@@deriving show]
  let to_tuple = N_tuples.flatten3
  let from_tuple = N_tuples.from3
  let show c = [%show: coord] (to_tuple c)
end

module Vec16: sig
  include module type of V(Four)
  type coord = Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t
  include RVec with type coord := coord and type c_list := C.t
end = struct
  include V(Four)
  type coord = Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t * Coord.HighLow.t [@@deriving show]
  let to_tuple = N_tuples.flatten4
  let from_tuple = N_tuples.from4
  let show c = [%show: coord] (to_tuple c)
end

let create_vec n =
  let lg_n = log2 n in
  let n_mod = create_nat lg_n in
  (module V(val n_mod): Vec_shuffle)

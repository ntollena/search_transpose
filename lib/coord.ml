open Batteries
open Utils
open Naturals

module HighLow = struct
  type t = High | Low [@@deriving show]

  let invert = function
    | High -> Low
    | Low -> High

  let cmp hl1 hl2 = match (hl1, hl2) with
    | High, High | Low, Low -> Eq
    | High, Low -> Bigger
    | Low, High -> Less

  let to_int = function
    | High -> 1
    | Low -> 0

  let from_int = function
    | 0 -> Low
    | 1 -> High
    | n -> failwith (Printf.sprintf "Cannot convert %d to a high/low value" n)

  let tuples n =
    let rec tuples gen n =
      let append_elem =
          (List.map (fun e -> Low::e) gen)
          @
          (List.map (fun e -> High::e) gen) in
      if n = 0 then gen
      else tuples append_elem (n - 1) in
    List.map List.rev (tuples [[]] n)
end
module HL = HighLow

module Coord(S: Nat) = struct
  open N_tuples
  (* Low, High, Low is (0, 1, 0) *)
  type t = (HL.t, S.n) tuple

  let coord_at_dim coordi d = at_dim coordi d

  let invert_dim coord dim = apply_at_dim coord HL.invert dim

  let coord_end_compl coord dim = remove coord dim

  let inject v n_tuple dim = inject n_tuple v dim

  let inject_inv inv v n_tuple dim = match inv with
    | None -> inject v n_tuple dim
    | Some inv -> apply_at_dim (inject v n_tuple dim) HL.invert inv

  let coord_from_int index =
    init (fun i -> (let pos = (1 lsl i) in
                    HighLow.from_int ((index land pos) lsr pos)))
      S.num

  let coord_to_int coord =
    coord
    |> mapi
    (* flip lsl i is just fun x -> (x << i)
     * %> is the composition operator *)
      (fun i -> HL.to_int %> ((Fun.flip (lsl)) i))
    |> fold (+) 0

  let all_coords =
    List.map (from_list S.num) (HighLow.tuples (from_nat S.num))
end

(* Module type definition - redundant with mli *)

module type Coord_t = sig
  open N_tuples
  module S : Nat
  type t = (HighLow.t, S.n) tuple
  val coord_at_dim: t -> int -> HighLow.t
  val coord_end_compl: t -> int -> (HighLow.t, S.n_) tuple
  val invert_dim: t ->  int -> t
  val inject: HighLow.t -> (HighLow.t, S.n_) tuple -> int -> (HighLow.t, S.n) tuple
  val inject_inv: int option -> HighLow.t -> (HighLow.t, S.n_) tuple -> int ->
    (HighLow.t, S.n) tuple
  val coord_from_int: int -> t
  val coord_to_int: t -> int
  val all_coords: t list
end

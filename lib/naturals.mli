open Utils

type zero = Zero
type 'n s = S of 'n
type _ nat = Z: zero nat | S: 'n nat -> 'n s nat

val from_nat:  'n nat -> int
val cmp: 'n1 nat -> 'n2 nat -> ordering

module type GNat = sig type n val num: n nat end
module Zero: GNat with type n = zero

module type Nat = sig type n_ type n = n_ s val num: n nat end
module Incr(N:sig type n val num: n nat end): Nat with type n_ = N.n and type n = N.n s
module One: module type of Incr(Zero)
module Two: module type of Incr(One)
module Three: module type of Incr(Two)
module Four: module type of Incr(Three)
module Five: module type of Incr(Four)

val create_gnat: int -> (module GNat)
val create_nat: int -> (module Nat)
val incr_nat: (module GNat with type n = 'n) -> (module Nat with type n_ = 'n)
val pred_nat: (module Nat with type n_ = 'n) -> (module GNat with type n = 'n)

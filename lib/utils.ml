module Either = struct
  type ('a, 'b) t = Left of 'a | Right of 'b [@@deriving show]

  let left = Left ()
  let right = Right ()
  let to_bool = function
    | Left _ -> true
    | Right _ -> false

  let choose eith a b = match eith with
    | Left _ -> a
    | Right _ -> b

  let get: 'a. ('a, 'a) t -> 'a = function
    | Left v | Right v -> v

  let map2 f g = function
    | Left v -> Left (f v)
    | Right v -> Right (g v)

  let map f = map2 f f
  let map_left f = map2 f Fun.id
  let map_right f = map2 Fun.id f
end

type ordering = Bigger | Eq | Less
let enumerate al = List.mapi (fun i a -> (i, a)) al

let fun_power f n =
  let rec iter n x = if n = 0 then x else iter (n - 1) (f x) in
  fun x -> iter n x
(* Arithmetics *)
let int_floor p q =
  if (p < 0) != (q < 0) && p mod q != 0
  then p / q - 1
  else p / q

let int_ceil p q =
  if p mod q = 0
  then int_floor p q
  else int_floor p q + 1

let log2 n =
  let rec lg2 n lg =
    if n = 1 then lg
    else if n mod 2 <> 0 then raise (Invalid_argument "Not a power of two")
    else lg2 (n / 2) (lg + 1) in
  lg2 n 0

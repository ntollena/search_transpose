open Naturals

type ('a, _) tuple =
  | Void: ('a, zero) tuple
  | Cons: 'a * ('a, 'n) tuple -> ('a, 'n s) tuple

val make: 'a -> 'n nat -> ('a, 'n) tuple
val init: (int -> 'a) -> 'n nat -> ('a, 'n) tuple
val from_list: 'n nat -> 'a list -> ('a, 'n) tuple

val from1: 'a -> ('a, One.n) tuple
val from2: 'a * 'a -> ('a, Two.n) tuple
val from3: 'a * 'a * 'a -> ('a, Three.n) tuple
val from4: 'a * 'a * 'a * 'a -> ('a, Four.n) tuple

val flatten1: ('a, One.n) tuple -> 'a
val flatten2: ('a, Two.n) tuple -> 'a * 'a
val flatten3: ('a, Three.n) tuple -> 'a * 'a * 'a
val flatten4: ('a, Four.n) tuple -> 'a * 'a * 'a * 'a

val map: ('a -> 'b) -> ('a, 'n) tuple -> ('b, 'n) tuple
val mapi: (int -> 'a -> 'b) -> ('a, 'n) tuple -> ('b, 'n) tuple
val map_until: ('a -> 'a) -> ('a, 'n) tuple -> int -> ('a, 'n) tuple
val apply: ('a -> 'b, 'n) tuple -> ('a, 'n) tuple -> ('b, 'n) tuple
val fold: ('acc -> 'a -> 'acc) -> 'acc -> ('a, 'n) tuple -> 'acc

val at_dim: ('a, 'n) tuple -> int -> 'a
val apply_at_dim: ('a, 'n) tuple -> ('a -> 'a) -> int -> ('a, 'n) tuple
val inject: ('a, 'n) tuple -> 'a -> int -> ('a, 'n s) tuple
val remove: ('a, 'n s) tuple -> int -> ('a, 'n) tuple

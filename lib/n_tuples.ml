open Naturals

type ('a, _) tuple =
  | Void: ('a, zero) tuple
  | Cons: 'a * ('a, 'n) tuple -> ('a, 'n s) tuple

let rec make: type n. 'a -> n nat -> ('a, n) tuple =
  fun e n -> match n with
    | Z -> Void
    | S n -> Cons(e, make e n)

let init =
  let rec init:type n. (int -> 'a) -> int -> n nat -> ('a, n) tuple =
    fun f count n -> match n with
      | Z -> Void
      | S n -> Cons(f count, init f (succ count) n) in
  fun f n -> init f 0 n

let rec from_list: type n. n nat -> 'a list -> ('a, n) tuple = 
  fun n l -> match n, l with
    | Z, [] -> Void
    | S n, h::t -> Cons(h, from_list n t)
    | _, _ -> raise (Invalid_argument "wrong list size")

let from1 e = Cons (e, Void)
let from2 (e1, e2) = Cons(e1, from1 e2)
let from3 (e1, e2, e3) = Cons(e1, from2 (e2, e3))
let from4 (e1, e2, e3, e4) = Cons(e1, from3 (e2, e3, e4))

let flatten1: ('a, zero s) tuple -> 'a = function
    Cons (e1, Void) -> e1

let flatten2: ('a, zero s s) tuple -> 'a * 'a = function
    Cons (e1, tuple) -> let e2 = flatten1 tuple in (e1, e2)

let flatten3: ('a, zero s s s) tuple -> 'a * 'a * 'a = function
    Cons (e1, two_tuple) -> let (e2, e3) = flatten2 two_tuple in (e1, e2, e3)

let flatten4: ('a, zero s s s s) tuple -> 'a * 'a * 'a * 'a = function
    Cons (e1, three_tuple) -> let (e2, e3, e4) = flatten3 three_tuple
    in (e1, e2, e3, e4)

let rec map: type n. ('a -> 'b) -> ('a, n) tuple -> ('b, n) tuple =
  fun f tuple -> match tuple with
    | Void -> Void
    | Cons (a, tuple) -> Cons (f a, map f tuple)

let mapi: type n. (int -> 'a -> 'b) -> ('a, n) tuple -> ('b, n) tuple =
  let rec mapi: type n. (int -> 'a -> 'b) -> int -> ('a, n) tuple -> ('b, n) tuple =
  fun f count tuple -> match tuple with
    | Void -> Void
    | Cons (a, tuple) -> Cons (f count a, mapi f (succ count) tuple) in
  fun f tuple -> mapi f 0 tuple

let rec map_until: type n. ('a -> 'a) -> ('a, n) tuple -> int
  -> ('a, n) tuple = fun f tuple n -> match n, tuple with
  | _, Void -> Void
  | 0, Cons (e, tuple) -> Cons(f e, tuple)
  | n, Cons (e, tuple) -> Cons(f e, map_until f tuple (pred n))

let rec apply: type n. ('a -> 'b, n) tuple -> ('a, n) tuple -> ('b, n) tuple =
  fun ftuple atuple -> match ftuple, atuple with
    | Void, Void -> Void
    | Cons (f, ftuple), Cons (a, atuple) -> Cons (f a, apply ftuple atuple)
    | _ -> .

let rec fold: type n. ('acc -> 'a -> 'acc) -> 'acc -> ('a, n) tuple -> 'acc =
  fun f acc tuple -> match tuple with
  | Void -> acc
  | Cons (elem, tuple) -> fold f (f acc elem) tuple

(* In this convention, outter dimension is dimension 0 of tuple *)
let rec at_dim: type n. ('a, n) tuple -> int -> 'a = fun tuple n ->
  match n, tuple with
  | 0,  Cons (e, _) -> e
  | n, Cons (_, tup) -> at_dim tup (n -1)
  | _, Void -> failwith "Out of bound"

let rec apply_at_dim: type n. ('a, n) tuple -> ('a -> 'a) -> 
  int -> ('a, n) tuple = fun tuple f dim -> match dim, tuple with
    | 0, Void -> Void
    | 0, Cons (e, tuple) -> Cons (f e, tuple)
    | n, Cons (e, tuple) -> Cons (e, apply_at_dim tuple f (pred n))
    | _, Void -> failwith "Out of bound"

let rec inject: type n. ('a, n) tuple -> 'a -> int -> ('a, n s) tuple = 
  fun tuple a dim -> match dim, tuple with
    | 0, Void -> Cons(a, Void)
    | 0, (Cons _ as cs) -> Cons(a, cs)
    | n, Cons (e, tuple) -> Cons (e, inject tuple a (pred n))
    | _, Void -> failwith "Out of bound"

let rec remove: type n. ('a, n s) tuple -> int -> ('a, n) tuple =
  fun tuple n -> match n, tuple with
    | 0, Cons (_, tuple) -> tuple
    | n, Cons (e, (Cons (_, _) as tuple)) -> Cons (e, remove tuple (pred n))
    | _, Cons (_, Void) -> failwith "Out of bound"

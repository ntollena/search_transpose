open Vectors

module type Mat_dim = sig
  val inner: int
  val outer: int
end

module type Check_t = sig
  type shuffle
  val print_vectors: shuffle -> unit
  val print_move: shuffle -> unit
  val is_transpose: shuffle -> bool
end

module Check(Vec:Vec_shuffle)(M:Mat_dim):
  Check_t with type shuffle = (Vec.vector_expr array -> Vec.vector_expr array)

(* Same as the previous one, but this time each
 * line in both input and ouput is required to fit
 * in a single vector, even if that makes this
 * vector incomplete *)
module Check_incomplete(Vec:Vec_shuffle)(M:Mat_dim):
  Check_t with type shuffle = (Vec.vector_expr array -> Vec.vector_expr array)

module Check4(M:Mat_dim): module type of Check(Vec4)(M)

module Check8(M:Mat_dim): module type of Check(Vec8)(M)

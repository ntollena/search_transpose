module Either: sig 
  type ('a, 'b) t = Left of 'a | Right of 'b [@@deriving show]
  val left : (unit, unit) t
  val right : (unit, unit) t
  val to_bool: ('a, 'b) t -> bool
  val choose: ('a, 'b) t -> 'c -> 'c -> 'c
  val get: ('a, 'a) t -> 'a
  val map: ('a -> 'b) -> ('a, 'a) t -> ('b, 'b) t
  val map2: ('a -> 'c) -> ('b -> 'd) -> ('a, 'b) t -> ('c, 'd) t
  val map_left: ('a -> 'c) -> ('a, 'b) t -> ('c, 'b) t
  val map_right: ('b -> 'd) -> ('a, 'b) t -> ('a, 'd) t
end

type ordering = Bigger | Eq | Less
val enumerate: 'a list -> (int * 'a) list

val fun_power: ('a -> 'a) -> int -> 'a -> 'a

val int_floor: int -> int -> int
val int_ceil: int -> int -> int
val log2: int -> int

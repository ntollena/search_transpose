open Utils

(* Nat type and modules *)
type zero = Zero
type 'n s = S of 'n
type _ nat = Z: zero nat | S: 'n nat -> 'n s nat

let rec from_nat: type n. n nat -> int = function
  | Z -> 0
  | S n -> succ (from_nat n)

let rec cmp: type n1 n2. n1 nat -> n2 nat -> ordering = fun n1 n2 ->
  match n1, n2 with
  | Z, Z -> Eq
  | S _, Z -> Bigger
  | Z, S _ -> Less
  | S n1, S n2 -> cmp n1 n2

(* General Naturals (potentially zero) *)
module type GNat = sig type n val num: n nat end
(* Non-negative natural numbers *)
module type Nat = sig type n_ type n = n_ s val num: n nat end

module Incr(N:GNat) : Nat with type n_ = N.n and type n = N.n s = struct
  type n_ = N.n
  type n = N.n s
  let num = S N.num
end

module Zero = struct type n = zero let num = Z end
module One = Incr(Zero)
module Two = Incr(One)
module Three = Incr(Two)
module Four = Incr(Three)
module Five = Incr(Four)

let incr_nat (type n) (module N: GNat with type n = n) =
  (module Incr(N): Nat with type n_ = N.n)

let bump_gnat (module N: GNat) =
  (module Incr(N): GNat)

let rec create_gnat: int -> (module GNat)  = function
  | n when n < 0 -> failwith "Negative number"
  | 0 -> (module Zero: GNat)
  | n -> bump_gnat (create_gnat (pred n))

let bump_nat (module N: Nat) =
  (module Incr(N) : Nat)

let rec create_nat: int -> (module Nat)  = function
  | n when n <= 0 -> failwith "Negative number"
  | 1 -> (module One: Nat)
  | n ->  bump_nat (create_nat (pred n)) 

let pred_nat (type n) (module N: Nat with type n_ = n) =
  (module struct
    type n = N.n_
    let num = let S pred_n = N.num in pred_n
  end : GNat with type n = N.n_)


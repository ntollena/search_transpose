open Utils
open Batteries
module HL = Coord.HighLow

module type Grp_param_t = 
sig val num_vec: int end

module G(V:Vectors.Vec_shuffle)(Grp_param: Grp_param_t) = struct
  include Grp_param
  type exch = {low_vec: int; low_half: HL.t; high_vec:int; high_half: HL.t} [@@deriving show]

  type base_shuf = {
    (* Dimension on which we are incrementing *)
    pivot_dim:int;
    (* This array maps a vector in output to a vector in input 
     * It should be of size num_vec
     * perm.(1) = low_vec = 0; low_half= Low; high_vec= 3; high_half= Low
     * reads: the low half of the second output vector of this permutation 
     * maps to the low half of the first vector of the input, and the high 
     * half of the second vector of the output maps to the low half of the 
     * fourth vector of the input *)
    perm: exch array; 
  } [@@deriving show]

  let base_vec_shuf {pivot_dim; perm; _} =
    fun input_vecs ->
    Array.map 
      (fun {low_vec; low_half; high_vec; high_half} ->
         let extract = V.Par_shuffle 
             {keep = pivot_dim; left = low_half; right = high_half; out = HL.Low} in
         V.Extract (input_vecs.(low_vec), input_vecs.(high_vec), extract)
      ) perm

  (* A step in a n * vec_size transposition *)
  let transpose_step pivot_dim =
    let pair_shuffle = List.init (num_vec / 2)
        (fun i -> {low_vec = 2 * i; low_half = HL.Low; high_vec = 2 * i + 1; high_half = HL.Low}) in
    let step_shuffle = if (num_vec mod 2 = 0) then []
      else [{low_vec = num_vec - 1; low_half = HL.Low; high_vec = 0; high_half = HL.High}] in
    let odd_shuffle = if (num_vec mod 2 = 0) then List.init (num_vec / 2 )
        (fun i -> {low_vec = 2 * i; low_half = HL.High; high_vec = 2 * i + 1; high_half = HL.High}) 
      else List.init (num_vec / 2)
        (fun i -> 
          {low_vec = 2 * i + 1; low_half = HL.High; high_vec = 2 * i + 2; high_half = HL.High}
        ) in
    let perm = pair_shuffle @ step_shuffle @ odd_shuffle
  |>  Array.of_list in
    {pivot_dim; perm}

  let gen_base_neutral pivot_dim num_vec =
    let id_exch i = {low_vec = i; low_half = HL.Low; high_vec = i; high_half = HL.High} in
    let perm = Array.init num_vec id_exch in
    {pivot_dim; perm}

  let base_inverse shuffle =
    let shuf_list = enumerate (Array.to_list shuffle.perm) in
      let find_half hl i = function
        | idx, {low_vec; low_half;_} when low_vec = i && low_half = hl -> 
            Some (idx, HL.Low)
        | idx, {high_vec; high_half;_} when high_vec = i && high_half = hl -> 
            Some (idx, HL.High)
        | _ -> None in
      let remake_exch id =
        let low_vec, low_half = List.find_map (find_half HL.Low id) shuf_list
        and high_vec, high_half = List.find_map (find_half HL.High id) shuf_list in
    {low_vec; low_half; high_vec; high_half;} in 
    let exch_array = 
      List.init num_vec Fun.id 
      |> List.map remake_exch 
      |> Array.of_list in
  {shuffle with perm = exch_array}

  (* invariant : each base_shuf should have the same number of vector *)
  type t = base_shuf list

  let inverse shuffle = List.rev (List.map base_inverse shuffle)

  let gen_vec_shuf shuffle: V.vector_expr array -> V.vector_expr array = List.fold_left 
      (fun vec_shuffle base_exch -> vec_shuffle %> (base_vec_shuf base_exch)) Fun.id shuffle

  let transpose = 
    List.init V.n_dims
      (fun dim -> transpose_step dim)
end

open Utils
open Naturals

module HighLow: sig
  type t = High | Low [@@deriving show]
  val invert: t -> t
  val cmp: t -> t -> ordering
  val to_int: t -> int
  val from_int: int -> t
  (* Returns the list of all possible n-tuples of t 
   * For example :
   * tuples 2 = [[1; 1]; [1;0]; [0;1]; [0;0]] *)
  val tuples: int -> t list list
end

module type Coord_t = sig
  open N_tuples
  module S : Nat
  type t = (HighLow.t, S.n) tuple
  (* Accessor for coord
   * Coord_at_dim (H, L, H) 1 returns L *)
  val coord_at_dim: t -> int -> HighLow.t
  (*Remove dimension dim from coord
   * For example, coord_end_compl (H, L, L) 0 returns (L, L),
   * coord_end_compl (L, H, L, H) 2 returns (L, H, H) *)
  val coord_end_compl: t -> int -> (HighLow.t, S.n_) tuple
  (* Invert value on dimension dim
   * For example invert (Low, High) 0 returns (High, High) *)
  val invert_dim: t ->  int -> t
  (* Add a dimension
   * inject L (H, H) 2 returns (H, H, L)
   * inject L (H, H) 1 returns (H, L, H) *)
  val inject: HighLow.t -> (HighLow.t, S.n_) tuple -> int -> (HighLow.t, S.n) tuple
  (* Add a dimension and inverse another one
   * The inverted dimension is taken respectively to the final dimensionality
   * inject_inv 1 L (H, H) 2 returns (H, L, L)
   * inject 2 L (H, H) 1 returns (L, H, L) *)
  val inject_inv: int option -> HighLow.t -> (HighLow.t, S.n_) tuple -> int ->
    (HighLow.t, S.n) tuple
  val coord_from_int: int -> t
  val coord_to_int: t -> int
  val all_coords: t list
end

module Coord: functor(N: Nat) -> sig
  include Coord_t with module S := N
end

open Batteries
open Vectors.Vec4

let first_step3x4 =
  let ext_0 = Par_shuffle {keep=0; left=Low; right=Low; out=Low} in
  (* 0 4 2 6 *)
  let t0 = Extract (Vector 0, Vector 1, ext_0) in
  let ext_1 = Par_shuffle {keep=0; left=High; right=Low; out= High} in
  (* 8 1 10 3 *)
  let t1 = Extract (Vector 0, Vector 2, ext_1) in
  let ext_2 = Par_shuffle {keep=0;left= High;right= High;out=Low } in
  (* 5 9 7 11 *)
  let t2 = Extract (Vector 1, Vector 2, ext_2) in
  [|t0; t1; t2|]

module Grps4_v3 = Shuffle_group.G(Vectors.Vec4)(struct let num_vec = 3 end)
let trans_grp_3x4 = Grps4_v3.(gen_vec_shuf transpose)
let trans_grp_4x3 = Grps4_v3.(gen_vec_shuf (inverse transpose))

module Grps4_v6 = Shuffle_group.G(Vectors.Vec4)(struct let num_vec = 6 end)
let trans_grp_6x4 = Grps4_v6.(gen_vec_shuf transpose)
let trans_grp_4x6 = Grps4_v6.(gen_vec_shuf (inverse transpose))


let transpose_3x4 vectors =
  assert (Array.length vectors = 3);
  let ext_0 = Par_shuffle {keep=0; left=Low; right=Low; out=Low} in
  (* 0 4 2 6 *)
  let t0 = Extract (vectors.(0), vectors.(1), ext_0) in
  let ext_1 = Par_shuffle {keep=0; left=High; right=Low; out= High} in
  (* 8 1 10 3 *)
  let t1 = Extract (vectors.(0), vectors.(2), ext_1) in
  let ext_2 = Par_shuffle {keep=0;left= High;right= High;out=Low } in
  (* 5 9 7 11 *)
  let t2 = Extract (vectors.(1), vectors.(2), ext_2) in
  let ext_0 = Par_shuffle {keep=1;left= Low;right= Low;out=Low } in
  (* 0 4 8 1 *)
  let tt0 = Extract (t0, t1, ext_0) in
  let ext_1 = Par_shuffle {keep=1;left= High;right= Low;out=High } in
  (* 5 9 2 6  *)
  let tt1 = Extract (t0, t2, ext_1) in
  let ext_2 = Par_shuffle {keep=1;left= High;right= High;out=Low } in
  (* 10 3 7 11 *)
  let tt2 = Extract (t1, t2, ext_2) in
  [|tt0; tt1; tt2|]

let transpose_3x4_incomplete vectors =
  assert (Array.length vectors = 3);
  let shuf1 = Par_shuffle {keep=1;left= Low;right= Low;out=Low } in
  let t02 = Extract (vectors.(0), vectors.(2), shuf1) in
  let shuf2 = Par_shuffle {keep=1;left= High;right= High;out=Low } in
  let t02_1 = Extract (vectors.(0), vectors.(2), shuf2) in
  let tinv1 = Invert_dim (vectors.(1), 1) in
  let ext_little = Par_shuffle {keep=0;left= Low;right= Low;out=Low } in
  let tt0 = Extract (t02, vectors.(1), ext_little) in
  let ext_little_h = Par_shuffle {keep=0;left= High;right= High;out=Low } in
  let tt1 = Extract (t02, vectors.(1), ext_little_h) in
  let tt2 = Extract (t02_1, tinv1, ext_little) in
  let tt3 = Extract (t02_1, tinv1, ext_little_h) in
  [|tt0; tt1; tt2; tt3|]

let transpose_4x3 vectors =
  assert (Array.length vectors = 3);
  let ext_0 = Par_shuffle {keep=1;left= Low;right= High;out=Low } in
  (* 0 1 6 7 *)
  let t0 = Extract (vectors.(0), vectors.(1), ext_0) in
  let ext_1 = Par_shuffle {keep=1;left= High;right= Low;out=Low } in
  (* 2 3 8 9 *)
  let t1 = Extract (vectors.(0), vectors.(2), ext_1) in
  let ext_2 = Par_shuffle {keep=1;left= Low;right= High;out=Low } in
  (* 4 5 10 11 *)
  let t2 = Extract (vectors.(1), vectors.(2), ext_2) in
  let ext_0 = Par_shuffle {keep=0;left= Low;right= High;out=Low } in
  (* 0 3 6 9 *)
  let tt0 = Extract (t0, t1, ext_0) in
  let ext_1 = Par_shuffle {keep=0;left= High;right= Low;out=Low } in
  (* 1 4 7 10 *)
  let tt1 = Extract (t0, t2, ext_1) in
  let ext_2 = Par_shuffle {keep=0;left= Low;right= High;out=Low } in
  (* 2 5 8 11*)
  let tt2 = Extract (t1, t2, ext_2) in
  [|tt0; tt1; tt2|]

open Vectors.Vec8

let transpose_unpack_8x8 vectors =
  assert (Array.length vectors = 8);
  let ext_ml = Same_end (1, Low, Low, (0, Low)) in
  let ext_mh = Same_end (1, High, High, (0, Low)) in
  (* 0 8 1 9 4 12 5 13 *)
  let t0 = Extract (vectors.(0), vectors.(1), ext_ml) in
  (* 2 10 3 11 6 14 7 15 *)
  let t1 = Extract (vectors.(0), vectors.(1), ext_mh) in
  (* 16 24 17 25 20 28 21 29 *)
  let t2 = Extract (vectors.(2), vectors.(3), ext_ml) in
  (* 18 26 19 27 22 30 23 31 *)
  let t3 = Extract (vectors.(2), vectors.(3), ext_mh) in
  (* 32 40 33 41 36 44 37 45 *)
  let t4 = Extract (vectors.(4), vectors.(5), ext_ml) in
  (* 34 42 35 43 38 46 39 47 *)
  let t5 = Extract (vectors.(4), vectors.(5), ext_mh) in
  (* 48 56 49 57 52 60 53 61 *)
  let t6 = Extract (vectors.(6), vectors.(7), ext_ml) in
  (* 50 58 51 59 54 62 55 63 *)
  let t7 = Extract (vectors.(6), vectors.(7), ext_mh) in
  let shuf_low_ext = Shuffle {keep= 1; inv=0;left=Low;
                              right = Low; out = (1, Low)} in
  let shuf_high_ext = Shuffle {keep= 1; inv=0; left=High;
                               right = High; out = (1, Low)} in
  (* 8 0 24 16 12 4 28 20 *)
  let r0 = Extract (t0, t2, shuf_low_ext) in
  (* 9 1 25 17 13 5 29 21 *)
  let r1 = Extract (t0, t2, shuf_high_ext) in
  (* 10 2 26 18 14 6 30 22 *)
  let r2 = Extract (t1, t3, shuf_low_ext) in
  (* 11 3 27 19 15 7 31 23 *)
  let r3 = Extract (t1, t3, shuf_high_ext) in
  (* 40 32 56 48 44 36 60 52 *)
  let r4 = Extract (t4, t6, shuf_low_ext) in
  (* 41 33 57 49 45 37 61 53 *)
  let r5 = Extract (t4, t6, shuf_high_ext) in
  (* 42 34 58 50 46 38 62 54 *)
  let r6 = Extract (t5, t7, shuf_low_ext) in
  (* 43 35 59 51 47 39 63 55 *)
  let r7 = Extract (t5, t7, shuf_high_ext) in
  let take_big_low = Shuffle {keep= 2; inv=0; left=Low; right=Low;
                                   out=(2, Low)} in
  let take_big_high = Shuffle {keep=2; inv=0; left=High; right=High;
                               out=(2, Low)} in
  (* 0 8 16 24 32 40 48 56 *)
  let t0 = Extract (r0, r4, take_big_low) in
  (* 1 9 17 25 33 41 49 57 *)
  let t1 = Extract (r1, r5, take_big_low) in
  (* 2 10 18 26 34 42 50 58 *)
  let t2 = Extract (r2, r6, take_big_low) in
  (* 3 11 19 27 35 43 51 59 *)
  let t3 = Extract (r3, r7, take_big_low) in
  (* 4 12 20 28 36 44 52 60 *)
  let t4 = Extract (r0, r4, take_big_high) in
  (* 5 13 21 29 37 45 53 61 *)
  let t5 = Extract (r1, r5, take_big_high) in
  (* 6 14 22 30 38 46 54 62 *)
  let t6 = Extract (r2, r6, take_big_high) in
  (* 7 15 23 31 39 47 55 63 *)
  let t7 = Extract (r3, r7, take_big_high) in
  [|t0; t1; t2; t3; t4; t5; t6; t7|]

let transpose_8x3 vectors =
  assert (Array.length vectors = 3);
  let ext_0 = Par_shuffle {keep=1;left= High;right= Low;out=Low } in
  (* 2 3 8 9 6 7 12 13 *)
  let t0 = Extract (vectors.(0), vectors.(1), ext_0) in
  let ext_1 = Par_shuffle {keep=2;left= Low;right= High;out=Low } in
  (* 0 1 2 3 12 13 14 15 *)
  let t1 = Extract (vectors.(0), vectors.(1), ext_1) in
  let ext_2 = Par_shuffle {keep=2;left= High;right= Low;out=Low } in
  (* 4 5 6 7 16 17 18 19 *)
  let t2 = Extract (vectors.(0), vectors.(2), ext_2) in
  let ext_3 = Par_shuffle {keep=2;left= Low;right= High;out=Low } in
  (* 8 9 10 11 20 21 22 23 *)
  let t3 = Extract (vectors.(1), vectors.(2), ext_3) in
  let ext_4 = Par_shuffle {keep=1;left= High;right= Low;out=Low } in
  (* 10 11 16 17 14 15 20 21 *)
  let t4 = Extract (vectors.(1), vectors.(2), ext_4) in
  let ext_12 = Par_shuffle {keep=1;left= Low;right= High;out=Low } in
  (* 0 1 6 7 12 13 18 19 *)
  let t12 = Extract (t1, t2, ext_12) in
  let ext_23 = Par_shuffle {keep=1;left= Low;right= High;out=Low } in
  (* 4 5 10 11 16 17 22 23 *)
  let t23 = Extract (t2, t3, ext_23) in
  let ext_04 = Par_shuffle {keep=2;left= Low;right= High;out=Low } in
  (* 2 3 8 9 14 15 20 21 *)
  let t04 = Extract (t0, t4, ext_04) in
  let ext_1_out_of_2l = Par_shuffle {keep=0;left= Low;right= High;out=Low } in
  let ext_1_out_of_2h = Par_shuffle {keep=0;left= High;right= Low;out=Low } in
  (* 0 3 6 9 12 15 18 21*)
  let r0 = Extract (t12, t04, ext_1_out_of_2l) in
  (* 1 4 7 10 13 16 19 22 *)
  let r1 = Extract (t12, t23, ext_1_out_of_2h) in
  (* 2 5 8 11 14 17 20 23 *)
  let r2 = Extract (t04, t23, ext_1_out_of_2l) in
  [|r0; r1; r2|]

let transpose_3x8 vectors =
  assert (Array.length vectors = 3);
  let litl_ll = Par_shuffle {keep=0;left= Low;right= Low;out=Low } in
  (* 0 8 2 10 4 12 6 14 *)
  let t0 = Extract (vectors.(0), vectors.(1), litl_ll) in
  let litl_lh = Par_shuffle {keep=0;left= High;right= Low;out=High } in
  (* 16 1 18 3 20 5 22 7 *)
  let t1 = Extract (vectors.(0), vectors.(2), litl_lh) in
  let litl_hh =  Par_shuffle {keep=0;left= High;right= High;out=Low } in
  (* 9 17 11 19 13 21 15 23 *)
  let t2 = Extract (vectors.(1), vectors.(2), litl_hh) in
  let middle_ll = Par_shuffle {keep=1;left= Low;right= Low;out=Low } in
  (* 0 8 16 1 4 12 20 5 *)
  let tt0 = Extract (t0, t1, middle_ll) in
  let middle_lli = Par_shuffle {keep=1;left= High;right= Low;out=High } in
  (* 9 17 2 10 13 21 6 14 *)
  let tt1 = Extract (t0, t2, middle_lli) in
  let middle_hh = Par_shuffle {keep=1;left= High;right= High;out=Low } in
  (* 18 3 11 19 22 7 15 23 *)
  let tt2 = Extract (t1, t2, middle_hh) in
  let big_ll = Par_shuffle {keep=2;left= Low;right= Low;out=Low } in
  (* 0 8 16 1 9 17 2 10 *)
  let f0 = Extract (tt0, tt1, big_ll) in
  let big_hli = Par_shuffle {keep=2;left= High;right= Low;out=High } in
  (* 18 3 11 19 4 12 20 5 *)
  let f1 = Extract (tt0, tt2, big_hli) in
  let big_hh = Par_shuffle {keep=2;left= High;right= High;out=Low } in
  (* 13 21 6 14 22 7 15 23 *)
  let f2 = Extract (tt1, tt2, big_hh) in
  [|f0; f1; f2|]

let transpose_5x8 vectors =
  assert (Array.length vectors = 5);
  (* Small dim shuffling *)
  let litl_ll = Par_shuffle {keep=0;left= Low;right= Low;out=Low } in
  let litl_hh =  Par_shuffle {keep=0;left= High;right= High;out=Low } in
  let litl_lh = Par_shuffle {keep=0;left= High;right= Low;out=High } in
  (* 0 8 2 10 4 12 6 14 *)
  let t0 = Extract (vectors.(0), vectors.(1), litl_ll) in
  (* 9 17 11 19 13 21 15 23 *)
  let t1 = Extract (vectors.(1), vectors.(2), litl_hh) in
  (* 16 24 18 26 20 28 22 30 *)
  let t2 = Extract (vectors.(2), vectors.(3), litl_ll) in
  (* 25 33 27 35 29 37 31 39 *)
  let t3 = Extract (vectors.(3), vectors.(4), litl_hh) in
  (* 32 1 34 3 36 5 38 7 *)
  let t4 = Extract (vectors.(0), vectors.(4), litl_lh) in
  (* Middle dim shuffling *)
  let middle_ll = Par_shuffle {keep=1;left= Low;right= Low;out=Low } in
  (* 0 8 16 24 4 12 20 28 *)
  let tt0 = Extract (t0, t2, middle_ll) in
  let middle_hli = Par_shuffle {keep=1;left= High;right= Low;out=High } in
  (* 25 33 2 10 29 37 6 14 *)
  let tt1 = Extract (t0, t3, middle_hli) in
  let middle_hh = Par_shuffle {keep=1;left= High;right= High;out=Low } in
  (* 11 19 27 35 15 23 31 39 *)
  let tt2 = Extract (t1, t3, middle_hh) in
  let middle_lli = Par_shuffle {keep=1;left= Low;right= Low;out=High } in
  (* 32 1 9 17 36 5 13 21 *)
  let tt3 = Extract (t1, t4, middle_lli) in
  (* 18 26 34 3 22 30 38 7  *)
  let tt4 = Extract (t2, t4, middle_hh) in
  (* Big dim shuffling *)
  let big_ll = Par_shuffle {keep=2;left= Low;right= Low;out=Low } in
  let big_hhi = Par_shuffle {keep=2;left= High;right= High;out=High } in
  let big_hli = Par_shuffle {keep=2;left= High;right= Low;out=High } in
  (* 0 8 16 24 32 1 9 17 *)
  let f0 = Extract (tt0, tt3, big_ll) in
  (* 25 33 2 10 18 26 34 3 *)
  let f1 = Extract (tt1, tt4, big_ll) in
  (* 11 19 27 35 4 12 20 28 *)
  let f2 = Extract (tt0, tt2, big_hli) in
  (* 36 5 13 21 29 37 6 14 *)
  let f3 = Extract (tt1, tt3, big_hhi) in
  (* 22 30 38 7 15 23 31 39 *)
  let f4 = Extract (tt2, tt4, big_hhi) in
  [|f0; f1; f2; f3; f4|]


let transpose_3x5 vectors =
  assert (Array.length vectors = 2);
  let ext_0 = Par_shuffle {keep=1;left= Low;right= High;out=Low } in
  (* 0 1 10 11 4 5 14 15 *)
  let t0 = Extract (vectors.(0), vectors.(1), ext_0) in
  let ext_1 = Shuffle {keep = 1; inv = 2; left = High; right = Low;
                       out = (2, High)} in
  (* 12 13 8 9 6 7 2 3 *)
  let t1 = Extract (vectors.(0), vectors.(1), ext_1) in
  let ext_2 = Shuffle {keep = 1; inv = 2; left = Low; right = High;
                       out = (1, High)} in
  (* 2 3 12 13 8 9 8 9 *)
  let t2 = Extract (vectors.(1), t1, ext_2) in
  let ext_3 = Shuffle {keep = 1; inv = 2; left = Low; right = High;
                       out = (2, Low)} in
  (* 4 5 0 1 6 7 2 3 *)
  let t3 = Extract (vectors.(0), vectors.(0), ext_3) in
  let ext_4 = Same_end (2, High, Low, (1, High)) in
  (* 8 9 4 5 10 11 6 7 *)
  let t4 = Extract (vectors.(0), vectors.(1), ext_4) in
  let ext_5 = Par_shuffle {keep=2;left= Low;right= High;out=Low } in
  (* 4 5 0 1 10 11 6 7 *)
  let t5 = Extract (t3, t4, ext_5) in
  let ext_6 = Par_shuffle {keep=2;left= Low;right= High;out=Low } in
  (* 0 1 10 11 6 7 2 3 *)
  let t6 = Extract (t0, t1, ext_6) in
  let ext_7 = Par_shuffle {keep=2;left= High;right= Low;out=High } in
  (* 12 13 8 9 4 5 14 15 *)
  let t7 = Extract (t0, t1, ext_7) in
  let ext_8 = Par_shuffle {keep=0;left= Low;right= High;out=Low } in
  (* 0 5 10 1 6 11 2 7 *)
  let t8 = Extract (t6, t5, ext_8) in
  (* 12 3 8 13 4 9 14 9 *)
  let t9 = Extract (t7, t2, ext_8) in
  [|t8; t9|]

module Grps16_v7 = Shuffle_group.G(Vectors.Vec16)(struct let num_vec = 7 end)
let trans_grp_7x16 = Grps16_v7.(gen_vec_shuf transpose)
let trans_grp_16x7 = Grps16_v7.(gen_vec_shuf (inverse transpose))

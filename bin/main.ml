open Search_transpose
open Check_transpose
open Examples

(*Module checking if the given 4-elements vector shuffling is a 4x3 -> 3x4 transposition *)
module Check4_4x3 = Check4(struct let inner = 3 let outer = 4 end)
(*Module checking if the given 4-elements vector shuffling is a 3x4 -> 4x3 transposition *)
module Check4_3x4 = Check4(struct let inner = 4 let outer = 3 end)

module Check4_6x4 = Check4(struct let inner = 4 let outer = 6 end)
module Check4_4x6 = Check4(struct let inner = 6 let outer = 4 end)
(*Module checking if the given 8-elements vector shuffling is a 8x8 -> 8x8 transposition *)
module Check8_square = Check8(struct let inner = 8 let outer = 8 end)
(*  Etc. *)
module Check8_8x3 = Check8(struct let inner = 3 let outer = 8 end)
module Check8_3x8 = Check8(struct let inner = 8 let outer = 3 end)
module Check8_5x8 = Check8(struct let inner = 8 let outer = 5 end)
module Check8_3x5 = Check8(struct let inner = 5 let outer = 3 end)
(*Module checking for an "ragged" transposition - each line gets its own vector *)
module Check4_3x4_inc = Check_incomplete(Vectors.Vec4)(struct let inner = 4 let outer = 3 end)

(* We can use V(Naturals.Four) instead of V16 and still be compatible *)
module Check16_7x16 = Check(Vectors.V(Naturals.Four))(struct let inner = 16 let outer = 7 end)
module Check16_16x7 = Check(Vectors.Vec16)(struct let inner = 7 let outer = 16 end)

let () =
  assert (Check8_square.(print_vectors transpose_unpack_8x8; is_transpose transpose_unpack_8x8));
  print_newline ();
  assert (Check4_4x3.is_transpose transpose_4x3);
  assert (Check4_3x4.is_transpose transpose_3x4);
  assert (Check4_3x4.(print_vectors trans_grp_3x4; is_transpose trans_grp_3x4));
  print_newline ();
  assert (Check4_4x3.(print_vectors trans_grp_4x3; is_transpose trans_grp_4x3));
  print_newline ();
  assert (Check4_6x4.(print_vectors trans_grp_6x4; is_transpose trans_grp_6x4));
  print_newline ();
  assert (Check4_4x6.(print_vectors trans_grp_4x6; is_transpose trans_grp_4x6));
  print_newline ();
  assert (Check8_8x3.(print_vectors transpose_8x3; is_transpose transpose_8x3));
  print_newline ();
  assert (Check8_3x8.(print_vectors transpose_3x8; is_transpose transpose_3x8));
  print_newline ();
  assert (Check8_3x5.(print_vectors transpose_3x5; is_transpose transpose_3x5));
  print_newline ();
  assert (Check8_5x8.(print_vectors transpose_5x8; is_transpose transpose_5x8));
  print_newline ();
  assert (Check4_3x4_inc.(print_vectors transpose_3x4_incomplete; is_transpose
                            transpose_3x4_incomplete));
  assert (Check16_7x16.(print_vectors trans_grp_7x16; is_transpose trans_grp_7x16));
  print_newline ();
  assert (Check16_16x7.(print_vectors trans_grp_16x7; is_transpose trans_grp_16x7));
  print_newline ();
